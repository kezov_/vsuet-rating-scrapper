const triggerSelect = require("../components/triggerSelect");
const triggerLesson = require("../components/triggerLesson");

const {
  RATING_LINK,
  GRID_ID,
} = process.env;

module.exports = async function (browser, { recordBookNum, facultyName, group }) {
  let page = await browser.newPage();

  try {
    console.log(`Navigating to ${RATING_LINK}...`);
    await page.goto(RATING_LINK);

    page.on('console', msg => {
      for (let i = 0; i < msg.args().length; ++i)
        console.log(`${i}: ${msg.args()[i]}`);
    });

    await triggerSelect(page, `#ctl00_ContentPage_cmbFacultets`, facultyName);

    await page.waitForNavigation({ waitUntil: 'networkidle0' })
    await triggerSelect(page, `#ctl00_ContentPage_cmbGroups`, group);

    await page.waitForNavigation({ waitUntil: 'networkidle0' })
    let lessonsHref = await page.evaluate(_ => {
      return Array.from(document.querySelectorAll(`#ctl00_ContentPage_ucListVedBox_Grid tr td a`), link => {
        return {
          href: link.href,
          text: link.innerText
        }
      })
    });

    let ratingArray = [];

    for await (const lesson of lessonsHref) {
      try {

        ratingArray.push(await triggerLesson(page, lesson, recordBookNum));

        await page.goBack();

      } catch(e) {
        console.log(e);
      }
    }

    return ratingArray;

  } catch (e) {
    console.log(e);
  } finally {
    await page.close()
  }
};
