const fs = require('fs');

module.exports = async (
  page,
  link,
  recordBookNum
) => {
  console.log(link);

  await page.goto(link.href)
  let data;

  try {
    data = await page.evaluate((linkName, bookNum) => {
      // #ucVedBox_tblVed tr.VedRow1 td:not(:nth-of-type(3)):not(:nth-of-type(2)):not(:nth-of-type(1)):not(:nth-of-type(n + 34)), #ucVedBox_tblVed tr.VedRow2 td:not(:nth-of-type(3)):not(:nth-of-type(2)):not(:nth-of-type(1)):not(:nth-of-type(n + 34))
      const studentSelector = '#ucVedBox_tblVed tr.VedRow1 td:nth-of-type(3), #ucVedBox_tblVed tr.VedRow2 td:nth-of-type(3)';
      const ratingSelector = 'td:not(:nth-of-type(3)):not(:nth-of-type(2)):not(:nth-of-type(1)):not(:nth-of-type(n + 34))';

      let findStudentTr = Array.from(document.querySelectorAll(studentSelector)).filter(student => {
        return student.innerHTML === bookNum
      })[0];

      let findStudentRating = Array.from(findStudentTr.parentNode.querySelectorAll(ratingSelector), rate => {
        return rate.innerHTML
      })

      return {
        name: linkName.text,
        data: findStudentRating
      };
    }, link, recordBookNum);

  } catch(e) {
    console.log(e)
  }

  console.log(data)

  return data;
}
