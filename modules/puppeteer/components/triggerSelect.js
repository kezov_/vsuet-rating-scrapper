module.exports = async (page, selector, answer) => {
  try {
    await page.$eval(selector, (el, currentAnswer) => {
      let selectElement = el;
      let selectOptions = Array.from(selectElement.children, ({ innerHTML, value }) => {
        return {
          name: innerHTML,
          value: value || 0
        }
      });

      let currentFaculty = selectOptions.find(fac => fac.name === currentAnswer)

      const event = new Event('change');

      selectElement.value = currentFaculty.value;
      selectElement.dispatchEvent(event);
    }, answer);
  } catch (e) {
    console.log(e)
  }
}
