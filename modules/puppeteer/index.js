const puppeteer = require('puppeteer');

async function startBrowser() {
  let browser;

  try {
    browser = await puppeteer.launch({
      args: ['--no-sandbox', '--disable-setuid-sandbox']
    });
  } catch (e) {
    console.log(`Error with initialize browser => ${e}`)
  }

  return browser;
}

module.exports = {
  startBrowser
}
