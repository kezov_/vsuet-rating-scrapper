const startScrapInfo = require('../../../puppeteer/methods/pageScrapper');

module.exports = (req, res, browser) => {

  console.log(req.body)

  startScrapInfo(browser, req.body)
    .then(r => {
      res
        .status(200)
        .send({
          status: true,
          data: r
        });
    })
    .catch(e => {

      res
        .status(500)
        .send({
          status: false,
          message: 'Произошла ошибка ' + e
        });

      console.log(e)
    })
}
