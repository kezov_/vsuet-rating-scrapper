const getRating = require('./rating/get');

module.exports = (
  app,
  browserInstance
) => {

  app.post('/api/rating/get', (req, res) => getRating(req, res, browserInstance));

}
