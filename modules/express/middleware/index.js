module.exports = (
  app
) => {
  app.use((req, res, next) => {
    res.header("Access-Control-Allow-Methods", "PATCH, GET, POST, OPTIONS, DELETE");
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Authorization, Content-Length, X-Requested-With, X-Site");

    //console.log(req.headers);

    if ('OPTIONS' === req.method) {
      res.status(200).end();  // For OPTIONS requests, a 200 response is sent immediately
    } else {
      next();  // Continues normal workflow
    }
  });
}