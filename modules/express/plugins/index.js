const helmet = require('helmet');
const bodyParser = require('body-parser');
const rateLimit = require('express-rate-limit');

const limiter = rateLimit({
  windowMs: 15 * 60 * 1000,
  max: 100
});

module.exports = (
  app
) => {
  app.use(helmet());
  app.use(helmet.hsts());
  app.use(helmet.noSniff());
  app.use(helmet.hidePoweredBy());
  app.use(bodyParser.json());
  app.use(limiter);
}