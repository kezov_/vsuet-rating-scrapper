const express = require('express');
const app = express();

// dep
const middlewares = require('./middleware/');
const plugins = require('./plugins/');
const routes = require('./routes/');

middlewares(app);
plugins(app);
routes(app);

module.exports = app;