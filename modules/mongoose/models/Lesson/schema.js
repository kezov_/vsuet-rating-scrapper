const {
  mongoose,
  mongoose: {
    Schema,
    Schema: {
      Types: {
        ObjectId,
      }
    }
  }
} = require('mongoose');


const lesson = new Schema({
  name: {
    type: String,
    required: true
  },
  menu: {
    ref: 'menu',
    type: ObjectId,
    required: true
  },
  createdBy: {
    ref: 'users',
    type: ObjectId,
    select: false,
    required: true
  },
  createdTime: {
    type: Date,
    required: true
  }
}, {
  collection: 'lessons'
});

module.exports = mongoose.model('lesson', lesson);