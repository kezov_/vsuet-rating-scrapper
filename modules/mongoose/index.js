const mongoose = require('mongoose');
const { DB_PORT } = process.env;

const instance = mongoose.connect(`mongodb://localhost:${DB_PORT}/vsuet-db`, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

module.exports = instance;