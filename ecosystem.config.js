module.exports = {
  apps: [
    {
      name: 'VSUET Rating scrapper',
      script: './index.js',
      node_args: '',
      watch: true,
      max_memory_restart: '500M',
      ignore_watch: ['node_modules', 'upload', 'mongodb', '.git', '.idea'],
      kill_timeout: '30000',
      autorestart: true,
      watch_options: {
        "followSymlinks": false
      }
    },
  ],
};
