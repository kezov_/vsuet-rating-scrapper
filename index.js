require('dotenv').config();
const https = require('https');
const fs = require('fs');
const app = require('./modules/express/');
const mongoDb = require('./modules/mongoose/');

const options = {
  key: fs.readFileSync('./key/privkey.pem'),
  cert: fs.readFileSync('./key/fullchain.pem'),
  dhparam: fs.readFileSync('./key/dh-strong.pem')
}

const server = https.createServer(options, app);

mongoDb.then(e => {
  console.log('MongoDB start!')
  server.listen(8646, async err => {
    if (err) {
      console.error(err);
    }

    console.log('Server start! Port: 8646');
  })
})
  .catch(err => {
    console.error(err)
  })



